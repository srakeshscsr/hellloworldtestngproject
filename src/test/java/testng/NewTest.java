package testng;

import org.testng.annotations.Test;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

public class NewTest {
	 @Test(priority=1)
	  public void f() {
		  System.out.println("Testing");
	  }
	  @BeforeTest
	  public void beforeTest() {
		  System.out.println("Testing2");
	  }

	  @AfterTest
	  public void afterTest() {
		  System.out.println("Testing3");
	  }
}
